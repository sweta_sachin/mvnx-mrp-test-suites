package SubscriberTests;

import java.util.concurrent.TimeUnit;


import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.Wait;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;


public class CreditTabTests  extends Common_Methods.CommonMethods{
	
	public static String amt= "1800";
	
	public static String assertamt= "-1800";
	
	public static String reportamt= "R1,800.00";
	
	public static String plan= "MRP 100";
	
	@BeforeClass()
	public void Start() throws InterruptedException
	{
		Login();
		SearchByMSISDN(msisdn);
		Thread.sleep(500);
	}
	
	@AfterClass(alwaysRun=true)
	public void stop() throws InterruptedException
	{
	 Thread.sleep(1000);
		driver.quit();			
	}
	
	@Test
	public void a_Click_contractUpgrade() throws InterruptedException
	{
		click_contract_upgrade();
		
	}
	
	@Test
	public void b_SelectrateplanOption()
	{
		SelectRateplanOption();
	}
	
	@Test
	public void c_ChooseRateplanfromDropdown() throws InterruptedException
	{
		Thread.sleep(1500);
		ChooseRatePlan();
		
	}
	
	@Test
	public void d_EnterHandsetAmount() throws InterruptedException
	{
		EnterHansdetAmount();
	}
	
	@Test
	public void e_ClickNexttoProceed() throws InterruptedException
	{
		Thread.sleep(500);
		ClickNext();
	}
	
	@Test
	public void f_SubmitRequest() throws InterruptedException
	{
		Thread.sleep(1500);
		Submit();
	}
	
	@Test
	public void g_CheckSuccessMsg() throws InterruptedException
	{
		Thread.sleep(1500);
		SuccessMsg();
	}
	

	@Test
	public void i_Check_CreditTab_Report() throws InterruptedException
	{
		Thread.sleep(500);
		 Click_CreditTab();
		 Thread.sleep(500);
		 click_TransactionDateColumn();
		 Thread.sleep(500);
		 clickSort();
		 Thread.sleep(500);
		 CheckAmountOnCreditTab();
	}
	
//////////////////////////////////////////////////////////////////////////////////////////////
//****************************** Method to click contract upgrade tab ********************
public void click_contract_upgrade() throws InterruptedException
{
	Wait<WebDriver> wait = new FluentWait<WebDriver>(driver)							
			.withTimeout(30, TimeUnit.SECONDS) 			
			.pollingEvery(5, TimeUnit.SECONDS) 			
			.ignoring(NoSuchElementException.class);

	WebElement attachment= driver.findElement(By.id("B74726229437391028"));
	
	Boolean name= attachment.isDisplayed();
	
	if(name==true)  {
		    
		wait.equals(name);
		
		Actions act= new Actions(driver);
		
		act.moveToElement(attachment).doubleClick().build().perform();
		
		Thread.sleep(1500);
	}
}
	
//**************************** Method to select rate plan option **************************
public void SelectRateplanOption()
{
	WebElement rateplan= driver.findElement(By.id("P103_HAS_NEW_RATE_PLAN_0"));

	rateplan.click();
	
}

//**************************** Method to choose rateplan from dropdown *******************
public void ChooseRatePlan()
{
	Select dropdown= new Select(driver.findElement(By.id("P103_NEW_RP_UID")));

	dropdown.selectByVisibleText(plan);
	
}
	
//**************************** Method to enter handset Amount *********************
public void EnterHansdetAmount()
{
	WebElement amount= driver.findElement(By.id("P103_HANDSET_AMOUNT"));

	amount.clear();
	
	System.out.println("Handset amount entered is :" +amt);
	
	amount.sendKeys(amt);
	
}
	
//****************************** Method to click next ******************************
public void ClickNext() throws InterruptedException
{
	WebElement rateplan= driver.findElement(By.id("B74697711142280452"));

	Actions act= new Actions(driver);
	
	act.moveToElement(rateplan).click().build().perform();
	
	Thread.sleep(1500);
	
	WebElement rateplan1= driver.findElement(By.id("B74697711142280452"));
	
	Actions act1= new Actions(driver);
	
	act1.moveToElement(rateplan1).click().build().perform();
	
}
	
//*************************** Method to click submit *********************************
public void Submit() throws InterruptedException
{
	WebElement sub= driver.findElement(By.id("B74698125700280453"));

	Actions act= new Actions(driver);
	
	act.moveToElement(sub).click().build().perform();
	
	Thread.sleep(1500);
	
}
	
//*********************** Method to check success message **************************
public void SuccessMsg()
{
	WebElement msg= driver.findElement(By.id("echo-message"));

	String MSG= msg.getText();
	
	String Exp_Msg= "Upgrade Successful. Upgrade Successful.";
	
	Assert.assertEquals(MSG, Exp_Msg);
	
}
	
//********************** Method to check credit tab balance on CRM ******************
public void CredittabBalanceOnCRM()
{
	String bal=driver.findElement(By.id("P2_CRTS_CURRENT_BALANCE")).getText();

	System.out.println("Amount on CRM tab is :" +bal);
	
	Assert.assertEquals(bal,assertamt);
	
}

//*************************** Method to click Credit tab ****************************	
public void Click_CreditTab()
{
	 WebElement CT=	driver.findElement(By.linkText("Credit Tab"));
	 
	 CT.click();
}	

//*************************** Method to click transaction date column *****************
public void click_TransactionDateColumn()
{
	WebElement TD=	driver.findElement(By.id("apexir_CRTSA_TRANSACTION_DATE"));

	Actions act= new Actions(driver);
	
	act.moveToElement(TD).click().build().perform();
	
}

//*************************** Method to click sort down arrow **********************
public void clickSort()
{
	WebElement sort=	driver.findElement(By.id("apexir_sortdown"));
	
	Actions act= new Actions(driver);
	
	act.moveToElement(sort).click().build().perform();
	
}

//******************************* Method to check amount on credit tab ****************
public void CheckAmountOnCreditTab()
{
	String bal=driver.findElement(By.xpath("//tr[2]//td[5]")).getText();

	System.out.println("Amount on credit tab report is :" +bal);
	
	Assert.assertEquals(bal,reportamt);
  }
}
